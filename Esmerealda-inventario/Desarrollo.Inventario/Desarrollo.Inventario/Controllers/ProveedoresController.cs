﻿using Desarrollo.Inventario.ModelosBASE;
using Desarrollo.Inventario.Models.ProveedoresModel;
using Desarrollo.Inventario.Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Westwind.Web.Mvc;

namespace Desarrollo.Inventario.Controllers
{
    public class ProveedoresController : Controller
    {
        ProveedoresService proveedoresService = new ProveedoresService();

       
        [Authorize]
        
        public ActionResult Index(proveedoresListVM _proveedoresListVM)
        {
            proveedoresListVM objproveedores = new proveedoresListVM() {
                    codigo= _proveedoresListVM.codigo,
                    nombre = _proveedoresListVM.nombre,
                    listadoproveedores=proveedoresService.listadoproveedores(_proveedoresListVM.codigo, _proveedoresListVM.nombre)

            };



            return View(objproveedores);
        }


        [Authorize]
        public JsonResult GuardarProveedor(string nombre, string direccion, string telefono, string correo)
        {
            bool exito = proveedoresService.GuardarProveedor(nombre, direccion, telefono, correo);

            List<Proveedores> proveedor = proveedoresService.listadoproveedores(null, "");

            proveedoresListVM objcat = new proveedoresListVM()
            {
                listadoproveedores = proveedor,

            };

            string postsHtml = ViewRenderer.RenderPartialView("~/Views/Proveedores/listadoProveedores.cshtml", objcat);

            return Json(new { exito = exito, postsHtml = postsHtml }, JsonRequestBehavior.AllowGet);
        }








    }
}