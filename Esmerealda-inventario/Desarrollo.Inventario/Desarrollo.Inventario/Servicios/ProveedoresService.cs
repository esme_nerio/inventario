﻿using Desarrollo.Inventario.ModelosBASE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Desarrollo.Inventario.Servicios
{
    public class ProveedoresService
    {
        DataProveedorDataContext _context = new DataProveedorDataContext();

        public List<Proveedores> listadoproveedores(int? codigo, string nombre) {

            List<Proveedores> listado = new List<Proveedores>();

            var listadoproveedores = _context.SP_LISTAR_PROVEEDORES(codigo, nombre);
            
            foreach(var proveedor in listadoproveedores)
            {

                Proveedores objproveedor = new Proveedores()
                {
                    codigo_proveedor = proveedor.codigo_proveedor,
                    correo=proveedor.correo,
                    direccion=proveedor.direccion,
                    nombre_proveedor=proveedor.nombre_proveedor,
                    telefono=proveedor.telefono

                };

                listado.Add(objproveedor);


            }

            return listado;
        }

        public bool GuardarProveedor(string nombre, string direccion,  string telefono, string correo)
        {
            int valor;
            bool resultado = false;
            int? reference = null;

            int guardar = _context.SP_AGREGAR_PROVEEDORES(nombre, direccion , telefono, correo, ref reference);

            if (guardar != 0 && guardar > 0)
            {
                resultado = true;
            }
            return resultado;
        }

        public int EliminarProveedor(int codprov)
        {
            int resultado = 0;

            try
            {
                _context.SP_ELIMINAR_PROVEEDORES(codprov);
                resultado = 1;

            }
            catch (Exception)
            {

                resultado = 2;

            }

            return resultado;

        }


    }
}